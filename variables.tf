variable "infix" {
  type        = string
  default     = "cla-tf-demo1"
  description = "Infix for naming resources"
}

variable "location" {
  type        = string
  default     = "westeurope"
  description = "Default Azure Region"
}
